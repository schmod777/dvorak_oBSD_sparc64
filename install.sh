#!/bin/sh
# Let's make US_Dvorak layout default at boot.
# to be executed as root;

file="/etc/rc.local"

cp ./dvorakd.sh /usr/local/bin/

if [ -f "$file" ]
then
	echo 'sh /usr/local/bin/dvorakd.sh' >> /etc/rc.local
else
	echo '#!/bin/sh' > /etc/rc.local
	echo 'sh /usr/local/bin/dvorakd.sh' >> /etc/rc.local
fi

# reboot the machine to let changes happen,
# or just ``$ sh ./dvorakd.sh'' to test it.
